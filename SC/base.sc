include("SuperDirt");



SuperDirt.start

MIDIClient.init;

(
// Midi para syths de LMMS
MIDIClient.init;
~midiOut1 = MIDIOut.newByName("LMMS", "from_another_galaxy");
~midiOut1.latency_(~dirt.server.latency);

~midiOut2 = MIDIOut.newByName("LMMS", "SuperSaw 2");
~midiOut2.latency_(~dirt.server.latency);

~midiOut3 = MIDIOut.newByName("LMMS", "LB302");
~midiOut3.latency_(~dirt.server.latency);

~midiOut4 = MIDIOut.newByName("LMMS", "Metal Sound 5");
~midiOut4.latency_(~dirt.server.latency);

~midiOut5 = MIDIOut.newByName("LMMS", "Passing Comet");
~midiOut5.latency_(~dirt.server.latency);

~midiOut6 = MIDIOut.newByName("LMMS", "from_new_arp3");
~midiOut6.latency_(~dirt.server.latency);

~midiOut7 = MIDIOut.newByName("LMMS", "analog strings");
~midiOut7.latency_(~dirt.server.latency);

~midiOut8 = MIDIOut.newByName("LMMS", "kick01.ogg");
~midiOut8.latency_(~dirt.server.latency);

~midiOut9 = MIDIOut.newByName("LMMS", "Pulse Pad 3");
~midiOut9.latency_(~dirt.server.latency);
)





(
~dirt.soundLibrary.addMIDI(\galaxy, ~midiOut1);

~dirt.soundLibrary.addMIDI(\saw3, ~midiOut2);

~dirt.soundLibrary.addMIDI(\lb302, ~midiOut3);

~dirt.soundLibrary.addMIDI(\metalico, ~midiOut4);

~dirt.soundLibrary.addMIDI(\comet, ~midiOut5);

~dirt.soundLibrary.addMIDI(\arp3, ~midiOut6);

~dirt.soundLibrary.addMIDI(\string, ~midiOut7);

~dirt.soundLibrary.addMIDI(\pala, ~midiOut8);

~dirt.soundLibrary.addMIDI(\pulsepad, ~midiOut9);
)


